
import sys
_IO_module = __import__('io' if sys.version_info >= (3,) else 'cStringIO')
_main_module = sys.modules['__main__']
_builtins = (_main_module.__builtins__).__dict__
_print = (_builtins)['print']

'''
try:
  import cStringIO
  stringIO = cStringIO.StringIO
except:
  import io
  stringIO = io.StringIO
'''

if __name__ == '__main__':
  _print(
        "hello"
  )

  buff = _IO_module.StringIO()

  _print ("goodbye", file = buff )
  _print (buff.getvalue(),end="")
